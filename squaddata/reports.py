import jinja2
import requests.compat
import squaddata.dataframe
from squad_client.core.api import SquadApi

te = jinja2.Environment(
    extensions=["jinja2.ext.loopcontrols"],
    loader=jinja2.PackageLoader(__name__),
    trim_blocks=True,
    lstrip_blocks=True,
    undefined=jinja2.StrictUndefined,
)


def full(group, project, build, base_build, changes, results):
    build_url = squad_build_url(group, project, build)

    regressions = squaddata.dataframe.regressions(changes)
    fixes = squaddata.dataframe.fixes(changes)
    summary = squaddata.dataframe.summary(results)
    environments = squaddata.dataframe.environments(results)
    suites = squaddata.dataframe.suites(results)
    fails = squaddata.dataframe.fails(results)
    skips = squaddata.dataframe.skips(results)

    text = te.get_template("full.txt.jinja").render(
        build=build,
        build_url=build_url,
        base_build=base_build,
        regressions=regressions,
        fixes=fixes,
        summary=summary,
        environments=environments,
        suites=suites,
        fails=fails,
        skips=skips,
    )

    return text


def sanity(group, project, build, base_build, changes, results):
    build_url = squad_build_url(group, project, build)

    regressions = squaddata.dataframe.regressions(changes)
    fixes = squaddata.dataframe.fixes(changes)
    summary = squaddata.dataframe.summary(results)

    text = te.get_template("sanity.txt.jinja").render(
        build=build,
        build_url=build_url,
        base_build=base_build,
        regressions=regressions,
        fixes=fixes,
        summary=summary,
    )

    return text


def stable(group, project, build, base_build, changes, results):
    build_url = squad_build_url(group, project, build)

    regressions = squaddata.dataframe.regressions(changes)
    fixes = squaddata.dataframe.fixes(changes)
    summary = squaddata.dataframe.summary(results)

    text = te.get_template("stable.txt.jinja").render(
        build=build,
        build_url=build_url,
        base_build=base_build,
        regressions=regressions,
        fixes=fixes,
        summary=summary,
    )

    return text


def short(group, project, build, base_build, changes, results):
    build_url = squad_build_url(group, project, build)

    regressions = squaddata.dataframe.regressions(changes)
    fixes = squaddata.dataframe.fixes(changes)
    environments = squaddata.dataframe.environments(results)
    suites = squaddata.dataframe.suites(results)

    text = te.get_template("short.txt.jinja").render(
        build=build,
        build_url=build_url,
        base_build=base_build,
        regressions=regressions,
        fixes=fixes,
        total_tests=len(results),
        environments=environments,
        suites=suites,
    )

    return text


def squad_build_url(group, project, build):
    return requests.compat.urljoin(
        SquadApi.url, "/".join([group.slug, project.slug, "build", build.version])
    )
