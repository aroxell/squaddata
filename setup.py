import pathlib
import setuptools

readme = pathlib.Path("README.md").read_text(encoding="utf-8")

setuptools.setup(
    name="squaddata",
    version="0.0.1",
    description="squad data",
    license="MIT",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jscook2345/squaddata",
    author="LKFT",
    author_email="lkft@linaro.org",
    packages=["squaddata"],
    include_package_data=True,
    python_requires=">=3.6, <4",
    install_requires=["jinja2", "pandas", "requests", "requests_cache", "squad-client"],
    entry_points={
        "console_scripts": [
            "squad_data=squaddata.cli:data",
            "squad_report=squaddata.cli:report",
        ]
    },
)
